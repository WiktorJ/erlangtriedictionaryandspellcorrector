%%%-------------------------------------------------------------------
%%% @author Marcin
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. sty 2015 20:58
%%%-------------------------------------------------------------------
-module(gameServer).
-author("Marcin").
-behaviour(gen_server).

%% API
-export([start_link/0, init/1, randomize/0, handle_call/3, handle_cast/2, checkTheCall/2, get_state/0, play/1, crash/0, stop/0, randomize/2, start/0, string_to_list/2]).

start_link () ->
  gen_server:start_link({local,gameServer},gameServer,{[],0,3},[]).

init({Game,L,B}) ->
  {ok, {Game,L,B}}.

randomize() ->
  gen_server:call(?MODULE,randomize).

start() ->
  randomize().

%%handlers


play(Word) ->
  {Game,L,B}=get_state(),
  {{Condition1,Msg},Condition2}={trieSup:check(Game,Word),trieSup:check_in_all_trees(Word)},
  case {{Condition1,Msg},Condition2} of
    {{ok,Msg},true} -> erlang:display(Msg),
      gen_server:call(?MODULE,point) ;
    {{error,Msg},true} -> erlang:display(Msg),
      gen_server:call(?MODULE,mistake);
    {{ok,Msg},false} -> erlang:display("This word does not exisits"),
      gen_server:call(?MODULE,mistake);
    {{error,Msg},false} -> erlang:display("This word does not exisits"),
      gen_server:call(?MODULE,mistake)
  end.

get_state() ->
  gen_server:call(?MODULE,getState).

handle_call(randomize,_FROM,{Game,L,B}) ->
  checkTheCall({Game,L,B},{randomize(16,lists:seq($A,$Z)),0,3});

handle_call(getState,_FROM,{Game,L,B}) ->
  checkTheCall({Game,L,B},{Game,L,B});

handle_call(point,_FROM,{Game,L,B}) ->
  checkTheCall({Game,L,B},{Game,L+1,B});

handle_call(mistake,_FROM,{Game,L,B}) ->
  checkTheCall({Game,L,B},{Game,L,B-1}).

handle_cast(stop,{Game,L,B}) ->
  {stop,normal,{Game,L,B}};
handle_cast(crash,{Game,L,B}) ->
  1/0,
  {noreply,{Game,L,B}}.

crash() ->
  gen_server:cast(gameServer,crash).

stop() ->
  gen_server:cast(gameServer,stop).

checkTheCall({Game,L,B},{error, Description}) ->
  {reply,{error, Description}, {Game,L,B}};
checkTheCall(_,{NGame,NL,NB}) ->
  erlang:display("-------------------"),
  erlang:display("-------------------"),
  case NB of
    0 -> erlang:display("you ran out of chances"),
        stop();
    _ ->
      L = string_to_list(NGame,[]),
      erlang:display(lists:sublist(L,4)),
      erlang:display(lists:sublist(L,5,4)),
      erlang:display(lists:sublist(L,9,4)),
      erlang:display(lists:sublist(L,13,16)),
      erlang:display("-------------------"),
      erlang:display("letters, points, chances")
  end,
  {reply,{NGame,NL,NB},{NGame,NL,NB}}.

randomize(Length, AllowedChars) ->
  lists:foldl(fun(_, Acc) ->
    [lists:nth(random:uniform(length(AllowedChars)),
      AllowedChars)]
    ++ Acc
  end, [], lists:seq(1, Length)).


string_to_list([],L)->
  string:tokens(L," ");
string_to_list([H|T],L) ->
  string_to_list(T,L ++ " " ++ [H]).
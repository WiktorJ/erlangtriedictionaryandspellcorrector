%%%-------------------------------------------------------------------
%%% @author Wiktor
%%% @copyright (C) 2015, <x`COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. Jan 2015 6:35 PM
%%%-------------------------------------------------------------------
-module(trieSup).
-author("Wiktor").
-behaviour(supervisor).

%% API
-export([start_link/0, init/1, trees_maker/1, make_trees/1, check_in_all_trees/1]).
-export([check/2]).

start_link() ->
  supervisor:start_link({local, trieSup}, ?MODULE, []).

init(_) ->
  {ok, {
    {one_for_one, 2, 2000},
    [{t1,
      {proper_srv, start_link, [t1, trie]},
      permanent, brutal_kill, worker, [proper_srv]},
      {t2,
        {proper_srv, start_link, [t2, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
      {t3,
        {proper_srv, start_link, [t3, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
      {t4,
        {proper_srv, start_link, [t4, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
      {t5,
        {proper_srv, start_link, [t5, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
      {t6,
        {proper_srv, start_link, [t6, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
      {t7,
        {proper_srv, start_link, [t7, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
      {t8,
        {proper_srv, start_link, [t8, trie]},
        permanent, brutal_kill, worker, [proper_srv]},
       {game,
        {gameServer, start_link, []},
        permanent, brutal_kill, worker, [gameServer]}
    ]}}.

trees_maker(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  A = binary:split(Binary, <<"\n">>, [global, trim]),
  List = [binary_to_list(X) || X <- A],
  Length = trunc(erlang:length(List) / 8),
  proper_srv:dict_from_list2(t1, lists:sublist(List, Length)),
  proper_srv:dict_from_list2(t2, lists:sublist(List, Length + 1, Length)),
  proper_srv:dict_from_list2(t3, lists:sublist(List, (2 * Length) + 1, Length)),
  proper_srv:dict_from_list2(t4, lists:sublist(List, (3 * Length) + 1, Length)),
  proper_srv:dict_from_list2(t5, lists:sublist(List, (4 * Length) + 1, Length)),
  proper_srv:dict_from_list2(t6, lists:sublist(List, (5 * Length) + 1, Length)),
  proper_srv:dict_from_list2(t7, lists:sublist(List, (6 * Length) + 1, Length)),
  proper_srv:dict_from_list2(t8, lists:sublist(List, (7 * Length) + 1, Length)).

make_trees(F) ->
  timer:tc(trieSup, trees_maker, [F]).

check_in_all_trees(Key) ->
  proper_srv:is_key(t1, Key) or proper_srv:is_key(t2, Key) or proper_srv:is_key(t3, Key) or proper_srv:is_key(t4, Key) or
proper_srv:is_key(t5, Key) or proper_srv:is_key(t6, Key) or  proper_srv:is_key(t7, Key) or proper_srv:is_key(t8, Key).


check(_,[]) -> {ok,"Well done"};
check([],_) -> {error,"Your word is too long"};
check(ListA, [H|T]) ->
  case lists:member(H,ListA) of
    true -> check(ListA -- [H], T);
    false -> {error,"You were supposed to use given letters"}
  end.




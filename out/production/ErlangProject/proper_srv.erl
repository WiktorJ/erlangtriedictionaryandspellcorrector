%-*-Mode:erlang;coding:utf-8;tab-width:4;c-basic-offset:4;indent-tabs-mode:()-*-
% ex: set ft=erlang fenc=utf-8 sts=4 ts=4 sw=4 et:
%%%
%%%------------------------------------------------------------------------
%%% @doc
%%% Test the dict API functions on an ordered collection.
%%% @end
%%%
%%% @author Michael Truog <mjtruog [at] gmail (dot) com>
%%% @copyright 2012 Michael Truog
%%%------------------------------------------------------------------------

-module(proper_srv).

-behaviour(gen_server).

%% external interface
-export([start_link/2,
  stop/0, from_list/1, readfile/1, dict_from_list/1, dict_from_list_store/2, is_key/2, dict_from_list2/2, size/1, crash/1]).

%% data structure API
-export([append/2,
         append_list/2,
         erase/1,
         fetch_keys/0,
         filter/1,
         find/1,
         fold/2,
         is_key/1,
         is_key/2,
         map/1,
         size/0,
         store/2,
         update/3,
         is_prefix/1,
         to_list/0]).

%% gen_server callbacks
-export([init/1,
         handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-record(state,
    {
        module,
        data_structure
    }).

%%%------------------------------------------------------------------------
%%% External interface functions
%%%------------------------------------------------------------------------

start_link(Name,Module) ->
    gen_server:start_link({local, Name}, ?MODULE, [Module], []).

stop() ->
    call(stop, undefined).

%%%------------------------------------------------------------------------
%%% Data structure API
%%%------------------------------------------------------------------------



append(Key, Value) ->
    call(append, [Key, Value]).

append_list(Key, L) ->
    call(append_list, [Key, L]).

erase(Key) ->
    call(erase, [Key]).

fetch_keys() ->
    call(fetch_keys, []).

filter(F) ->
    call(filter, [F]).

find(Key) ->
    call(find, [Key]).

fold(F, Acc) ->
    call(fold, [F, Acc]).

is_key(Key) ->
    call(is_key, [Key]).

is_key(Id, Key) ->
    call(Id, is_key, [Key]).

is_prefix(Prefix) ->
    call(is_prefix, [Prefix]).

to_list() ->
  call(to_list,[]).

from_list(L) ->
  call(from_list,[L]).

map(F) ->
    call(map, [F]).

size() ->
    call(size, []).

size(Id) ->
    call(Id,size, []).

store(Id,Key) ->
    call(Id,store, [Key]).

crash(Id) ->
  gen_server:cast(Id,crash).


update(Key, F, L) ->
    call(update, [Key, F, L]).

%%%------------------------------------------------------------------------
%%% Callback functions from gen_server
%%%------------------------------------------------------------------------

init([Module]) ->
    {ok, #state{module = Module,
                data_structure = Module:new()}}.


handle_call({stop, undefined}, _F, S) ->
    {stop, normal, ok, S};
handle_call({F, A}, _,
            #state{data_structure = D,
                   module = M} = S) when F =:= filter ->
    Result = erlang:apply(M, F, A ++ [D]),
    {reply, M:to_list(Result), S};
handle_call({F, A}, _,
            #state{data_structure = D,
                   module = M} = S) when F =:= fetch_keys;
                                         F =:= find;
                                         F =:= fold;
                                         F =:= is_key;
                                         F =:= is_prefix;
                                         F =:= to_list;
                                         F =:= size ->
    Result = erlang:apply(M, F, A ++ [D]),
    {reply, Result, S};

handle_call({F, A}, _,
            #state{data_structure = D,
                   module = M} = S) ->
  Result = M:to_list(D),
  NewD = erlang:apply(M, F, A ++ [D]),
  {reply, ok   , S#state{data_structure = NewD}};

handle_call(_, _, State) ->
{stop, invalid_call, error, State}.

handle_cast(crash, Id) ->
  1/0,
  {noreply,Id};


handle_cast({F, A}, #state{data_structure = D, module = M} = S) ->

  Result = M:to_list(D),
 NewD = erlang:apply(M, F, A ++ [D]),
{noreply, S#state{data_structure = NewD}};

handle_cast(_Msg, State) ->
  {stop, invalid_cast, State}.

handle_info(_Info, State) ->
{stop, invalid_info, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%------------------------------------------------------------------------
%%% Private functions
%%%------------------------------------------------------------------------

call(Command, Args) ->
  gen_server:call(?MODULE, {Command, Args}, infinity).

call(Id, Command, Args) ->
  gen_server:call(Id, {Command, Args}, infinity).

readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
   A = binary:split(Binary,<<"\n">>,[global,trim]),
  [binary_to_list(X)||X<-A].

dict_from_list(FileName) ->
  List = readfile(FileName),
  erlang:display(erlang:length(List)),
 [append(X,X)||X<-List].

dict_from_list_store(Id,FileName) ->
  List = readfile(FileName),
  [store(Id,X)||X<-List].

dict_from_list2(Id,List) ->
  [store(Id,X)||X<-List].


